# Data Mining Project
## Directories
- `data` stores our datasets, including visitors, restaurants and weather data.
- `examples` stores some example code (e.g. `folium` map sample code).
- `jupyter` you know:)
- `ressults` stores any data analysis results including data visualisation code.
- `members` stores individual member's code. Project members can put any thing they
  want in terms of **code**

## Notes
- It is not recommended that two people modify (update or delete) a same file simultaneously, it will cause merge conflicts. That is why `members` directory exists.
- Please check `Gitlab` when you are free :) Members may upload or update their file/code quite regularly.

## Use
- `git status`: Check the local repo status.
- `git log`: Check the commitment logs.
- `git pull`: Pull the latest repo. Merge your local repo and remote repo together.
- `git add <filename>`: Add your files that you want to upload to the stage ready to push to the remote repo.
- `git commit -m '<your_commit_message>'`: 
    - Commit your changes. 
    - The message is used for logging so members can see what changes. 
    - The committed changes will be then persistent in your local repo.
- `git push` / `git push origin master`: Push the committed changes to the remote repo.
- `git add`, `git commit` and `git push` should be done in sequence.
