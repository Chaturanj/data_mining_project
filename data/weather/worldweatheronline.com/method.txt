
Method employed to create the Tokyo weather csv file
====================================================

- Subscribe to www.worldweatheronline.com & obtain API key for historic REST web services.
- On a per month basis obtain raw JSON weather responses as per included URL.

http://api.worldweatheronline.com/premium/v1/past-weather.ashx?key=<API-KEY>&q=Tokyo&format=json&date=2016-01-01&enddate=2017-06-01

The api call above seems to cap the response to a single month of dates. Changing the start date permits cycling through the months between 01012016 and 31052017.

- Convert json to csv. I used http://json-csv.com
- Manually cleanup the monthly csv files. I remove redundant columns and performed some date value copies down through this column.
- Merge monthly csv files into a single file. I used http://merge-csv.com

