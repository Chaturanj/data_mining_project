copied from trello

Hi team,
Below are some interesting articles about forecasting for restaurants, some focus more on the weather than others.

- This one is from one previous competitor in the same competition as ours:
https://medium.com/@from2026/forecasting-restaurant-visits-silver-medal-solution-on-kaggle-8b1eb2f66a84
PredictingFutureVisitorsOfRestaurantsUsingBigData.pdf
- Also from a previous competitor
https://github.com/kasuo46/Restaurant_Visitor_Forecasting/blob/master/main_v1.py
- How Does Weather Impact Restaurants? Examples, Data, and More
https://pos.toasttab.com/blog/weather-impact-restaurants
- Restaurant visitor forecasting
http://dataanalyticsedge.com/case-studies/restaurant-visitor-forecasting/
- Forecasting methodology and process for restaurants
https://medium.com/@tenzoinc/forecasting-methodology-and-process-for-restaurants-66dc7325744f
- Keys to Making Accurate Sales Forecasts
https://www.foodnewsfeed.com/fsr/vendor-bylines/keys-making-accurate-sales-forecasts
- To Survive in Tough Times, Restaurants Turn to Data-Mining
https://www.nytimes.com/2017/08/25/dining/restaurant-software-analytics-data-mining.html
- predictive-ordering-platforms-for-restaurants
https://streetfightmag.com/2016/09/06/5-predictive-ordering-platforms-for-restaurants/
